FROM node:lts-alpine

# Install  simple http server
RUN yarn global add http-server

# Move directory
WORKDIR /app

# Copy package.json and yarn.lock in /app
COPY package.json ./
COPY yarn.lock ./

# Install dependencies
RUN yarn install

# Copy all files in /app
COPY . . 

# Build app
RUN yarn build

# Expose port
EXPOSE 8080

# Excecute commands
CMD [ "http-server", "dist" ]